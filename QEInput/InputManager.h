#pragma once

#include <vector>
#include "Keyboard.h"

namespace QEInput
{
	class InputManager
	{
	public:
		// Constructor
		InputManager(void);
		// Destructor
		~InputManager(void);
		// Update all input devices
		void Update(void);

		Keyboard keyboard;
	private:
	};
}