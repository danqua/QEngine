#pragma once

#include <map>

namespace QEInput
{
	class InputDevice
	{
	protected:
		std::map<int, bool> continuousButtons;
		std::map<int, bool> pressedButtons;
		std::map<int, bool> releasedButtons;
	public:
		// Button down event
		void OnButtonDown(int code);
		// Button up event
		void OnButtonUp(int code);
		// Resets the current button state
		void OnFrameEnd(void);
		// Returns true if button is pressed
		bool ButtonPressed(int code);
		// Returns true if button was released
		bool ButtonReleased(int code);
		// Returns true if button is held
		bool ButtonContinuous(int code);
	};
}