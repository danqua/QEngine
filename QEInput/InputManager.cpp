#include "InputManager.h"
#include "InputDevice.h"

#include <SDL.h>

namespace QEInput
{

	InputManager::InputManager(void)
	{

	}

	InputManager::~InputManager(void)
	{

	}
	
	void InputManager::Update(void)
	{
		SDL_Event ev;

		while (SDL_PollEvent(&ev))
		{
			SDL_SCANCODE_0;
			if (ev.type == SDL_KEYDOWN)
				keyboard.OnButtonDown(ev.key.keysym.scancode);
			if (ev.type == SDL_KEYUP)
				keyboard.OnButtonUp(ev.key.keysym.scancode);
		}
	}

}