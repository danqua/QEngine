#include "InputDevice.h"

namespace QEInput
{
	void InputDevice::OnButtonDown(int code)
	{
		pressedButtons[code]	= true;
		continuousButtons[code] = true;
	}

	void InputDevice::OnButtonUp(int code)
	{
		releasedButtons[code]	= true;
		continuousButtons[code] = false;
	}

	void InputDevice::OnFrameEnd(void)
	{
		pressedButtons.clear();
		releasedButtons.clear();
	}

	bool InputDevice::ButtonPressed(int code)
	{
		return pressedButtons[code];
	}

	bool InputDevice::ButtonReleased(int code)
	{
		return releasedButtons[code];
	}

	bool InputDevice::ButtonContinuous(int code)
	{
		return continuousButtons[code];
	}
}