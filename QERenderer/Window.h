#pragma once

#include "Interfaces/BaseWindow.h"

namespace QEngine { namespace Renderer {

	class Window : public BaseWindow
	{
	public:
		// Constructor
		Window(void);
		// Destructor
		~Window(void);

		// Initializes the render device
		virtual void Init(const std::string& sTitle, int iWidth, int iHeight) override;
		// Shuts down the render device
		virtual void Terminate(void) override;
		// Returns true if the render device is running
		virtual bool IsRunning(void);
		// Swaps buffer
		void SwapBuffers(void);
	};
} }