#pragma once

#include "Math/Vector.h"

namespace QEngine { namespace Renderer {

	struct Transform
	{
		Vec3 position	= Vec3(0.0f, 0.0f, 0.0f);
		Vec3 rotation	= Vec3(0.0f, 0.0f, 0.0f);
		Vec3 scale		= Vec3(1.0f, 1.0f, 1.0f);
	};

} }