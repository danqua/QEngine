#pragma once

#include "Interfaces/BaseImage.h"

namespace QEngine { namespace Renderer {

	class Image : public BaseImage
	{
	public:
		Image(void);
		~Image(void);

		virtual bool LoadFromFile(const std::string& sFilename) override;
	};
} }