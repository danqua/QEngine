#include "Image.h"

#include <string>
#include <SDL_image.h>

namespace QEngine { namespace Renderer {

	Image::Image(void)
	{
		// Load support for jpg and png
		int flags = IMG_INIT_JPG | IMG_INIT_PNG;
		int success = IMG_Init(flags);
		
		if ((success & flags) != flags)
			throw std::runtime_error("[Image] Failed to initalize SDL_Image: " + std::string(IMG_GetError()));
	}

	Image::~Image(void)
	{
		IMG_Quit();
	}

	bool Image::LoadFromFile(const std::string& sFilename)
	{
		SDL_Surface* image = IMG_Load(sFilename.c_str());

		if (!image)
		{
			// TODO Load default image
			throw std::runtime_error("[Image] Failed to load image: " + sFilename);
			return false;
		}

		m_Width			= image->w;
		m_Height		= image->h;
		m_BytesPerPixel = image->format->BytesPerPixel;
		int byteSize = m_Width * m_Height * m_BytesPerPixel;
		m_Data = (unsigned char*)malloc(byteSize);
		
		memcpy(m_Data, image->pixels, byteSize);

		return true;
	}

} }