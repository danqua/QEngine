#pragma once

#include <vector>
#include "BaseBuffer.h"
#include "BaseVertexBuffer.h"

namespace QEngine { namespace Renderer {

	class BaseVertexArray : public BaseBuffer
	{
	public:
		/**
		* Default virtual destructor
		*/
		virtual ~BaseVertexArray(void) = default;

		/**
		 * Init
		 *
		 * Initializes the vertex buffer object.
		 *
		 * @param data Vertex data
		 * @param size The size in bytes
		 * @param numComponents The number of components per vertex
		 */
		virtual void Init(void) = 0;
		
		/**
		 * AddBuffer
		 *
		 * Adds a vertex buffer to this vertex array object.
		 *
		 * @param pVertexBuffer Interface to the vertex buffer object
		 * @param index Index for the shader attribute location
		 */
		virtual void AddVertexBuffer(BaseVertexBuffer* pVertexBuffer, unsigned int index) = 0;
	
	protected:
		/** Stores all vertex buffers attachted to this vertex array */
		std::vector<BaseVertexBuffer*> m_VertexBuffers;
	};
} }