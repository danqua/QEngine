#pragma once

#include <string>

namespace QEngine { namespace Renderer {

	class BaseImage
	{
	public:
		/**
		 * Default destructor
		 */
		virtual ~BaseImage(void) = default;

		/**
		 * LoadImageFromFile
		 *
		 * @param sFilepath The image filepath
		 */
		virtual bool LoadFromFile(const std::string& sFilename) = 0;

		/**
		* GetWidth
		*
		* @return The width of the image
		*/
		inline const int& GetWidth(void) const { return m_Width; }

		/**
		* GetHeight
		*
		* @return The width of the image
		*/
		inline const int& GetHeight(void) const { return m_Height; }

		/**
		* GetBytesPerPixel
		*
		* @return The width of the image
		*/
		inline const unsigned char& GetBytesPerPixel(void) const { return m_BytesPerPixel; }

		/**
		* GetData
		*
		* @return The width of the image
		*/
		inline const unsigned char* GetData(void) const { return m_Data; }

	protected:
		/** Image width */
		int m_Width;
		/** Image height */
		int m_Height;
		/** Bytes per pixel */
		unsigned char m_BytesPerPixel;
		/** Image data */
		unsigned char* m_Data;
	};
} }