#pragma once

#include <string>

namespace QEngine { namespace Renderer {

	class BaseTexture
	{
	public:
		/**
		 * Default destructor
		 */
		virtual ~BaseTexture(void) = default;
		/**
		 * LoadFromFile
		 *
		 * Loads a texture from file
		 *
		 * @param filename The filepath the texture
		 */
		virtual void LoadFromFile(const std::string& sFilename) = 0;

		/**
		 * Bind
		 *
		 * Binds the texture
		 *
		 * @param slot If there are multiple active textures
		 */
		virtual void Bind(unsigned int slot = 0) const = 0;

		/**
		 * Unbind
		 *
		 * Unbinds the current textures
		 */
		virtual void Unbind(void) const = 0;

		/**
		* GetWidth
		*
		* @return The width of the image
		*/
		inline const int& GetWidth(void) const { return m_Width; }

		/**
		* GetHeight
		*
		* @return The width of the image
		*/
		inline const int& GetHeight(void) const { return m_Height; }

	protected:
		/** Texture id */
		unsigned int m_TextureId;
		/** Width of the texture */
		int m_Width;
		/** Height of the texture */
		int m_Height;
	};
} }