#pragma once

#include <string>
#include <map>
#include "Math/Matrix.h"
#include "Math/Vector.h"

namespace QEngine { namespace Renderer {

#define SHADER_VERTEX_ATTRIBUTE	0
#define SHADER_UV0_ATTRIBUTE	1
#define SHADER_UV1_ATTRIBUTE	2
#define SHADER_UV2_ATTRIBUTE	3
#define SHADER_UV3_ATTRIBUTE	4
#define SHADER_NORMAL_ATTRIBUTE	5
#define SHADER_COLOR_ATTRIBUTE	5

	class BaseShader
	{
	public:
		/**
		 * Default destructor
		 */
		virtual ~BaseShader(void) = default;
		
		/**
		 * Load
		 *
		 * @param sVertexShaderFilename The path to the vertex shader source
		 * @param sFragmentShaderFilename The path to the fragment shader source
		 */
		virtual void Load(const std::string& sVertexShaderFilename, const std::string& sFragmentShaderFilename) = 0;

		/**
		* Bind
		*
		* Binds the shader program
		*/
		virtual void Bind(void) const = 0;

		/**
		* Unbind
		*
		* Unbinds the shader program
		*/
		virtual void Unbind(void) const = 0;

		/**
		 * AddUniformLocation
		 *
		 * Adds a uniform location to the shader program
		 *
		 * @param sName The name of the uniform
		 */
		virtual void AddUniformLocation(const std::string& sName) = 0;

		/**
		 * SetUniformLocationMat4
		 *
		 * @param sName The name of the uniform
		 * @param pData Void pointer for user defined 4x4 matrix data 
		 */
		virtual void SetUniformLocationMat4(const std::string& sName, const Mat4& data) = 0;

		/**
		* SetUniformLocationVec3
		*
		* @param sName The name of the uniform
		* @param pData Void pointer for user defined vector 3 data
		*/
		virtual void SetUniformLocationVec3(const std::string& sName, const Vec3& data) = 0;

		/**
		* SetUniformLocationVec2
		*
		* @param sName The name of the uniform
		* @param pData Void pointer for user defined vector 2 data
		*/
		virtual void SetUniformLocationVec2(const std::string& sName, const Vec2& data) = 0;

		/**
		* SetUniformLocationFloat
		*
		* @param sName The name of the uniform
		* @param pData Void pointer for user defined float data
		*/
		virtual void SetUniformLocationFloat(const std::string& sName, const float& data) = 0;

		/**
		* SetUniformLocationInteger
		*
		* @param sName The name of the uniform
		* @param pData Void pointer for user defined int data
		*/
		virtual void SetUniformLocationInteger(const std::string& sName, const int& data) = 0;

	protected:
		/** Shader program id */
		unsigned int m_Program;
		/** Hashmap for uniform location names */
		std::map<std::string, unsigned int> m_UniformLocations;
	};

} }