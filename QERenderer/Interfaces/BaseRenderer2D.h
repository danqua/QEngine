#pragma once

#include "BaseRenderable2D.h"

namespace QEngine { namespace Renderer {
	
	class BaseRenderer2D
	{
	public:
		/**
		 * Default destructor
		 */
		~BaseRenderer2D(void) = default;

		/**
		* Draw
		*
		* @param pObject Renderable2D object to draw
		*/
		virtual void Draw(BaseRenderable2D* pObject) = 0;

		/**
		 * DrawLine
		 */
		//void DrawLine(float x0, float y0, float x1, float y1, float fThickness, uint32 uColor = 0xffffff);
	};
} }