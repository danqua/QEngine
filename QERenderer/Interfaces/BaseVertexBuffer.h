#pragma once

#include "BaseBuffer.h"

namespace QEngine { namespace Renderer {

	class BaseVertexBuffer : public BaseBuffer
	{
	public:
		/**
		* Default virtual destructor
		*/
		virtual ~BaseVertexBuffer(void) = default;

		/**
		 * Init
		 *
		 * Initializes the vertex buffer object.
		 *
		 * @param data Vertex data
		 * @param size The size in bytes
		 * @param numComponents The number of components per vertex
		 */
		virtual void Init(void* data, int size, unsigned int numComponents) = 0;
		
		/**
		* GetCount
		*
		* @return Number of components
		*/
		unsigned int GetComponentCount(void) const { return m_NumComponents; }

	protected:
		/** Number of components per vertex */
		unsigned int m_NumComponents;
	};
} }