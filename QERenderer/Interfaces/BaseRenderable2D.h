#pragma once

#include "Math/Vector.h"
#include "Math/Matrix.h"
#include "BaseVertexArray.h"
#include "BaseIndexBuffer.h"

namespace QEngine { namespace Renderer {



	class BaseRenderable2D
	{
	public:
		/**
		 * Default constructor
		 */
		virtual ~BaseRenderable2D() = default;

		/**
		 * Update
		 */
		virtual void Update(float fDelta = 1.0f) = 0;

		/**
		 * BindVertexArray
		 */
		void BindVertexArray(void) { m_pVertexArray->Bind(); }

		/**
		 * UnbindVertexArray
		 */
		void UnbindVertexArray(void) { m_pVertexArray->Unbind(); }

		/**
		 * BindIndexBuffer
		 */
		void BindIndexBuffer(void) { m_pIndexBuffer->Bind(); }

		/**
		 * UnbindIndexBuffer
		 */
		void UnbindIndexBuffer(void) { m_pIndexBuffer->Bind(); }

		/**
		 * IndexBufferCount
		 */
		inline int GetIndexBufferCount(void) const { return m_pIndexBuffer->GetCount(); }

		/**
		 * GetModelMatrix
		 */
		inline Mat4 GetModelMatrix(void) const { return m_Model; }
		
	protected:
		/** Vertex array object */
		BaseVertexArray* m_pVertexArray;
		/** Index buffer object */
		BaseIndexBuffer* m_pIndexBuffer;
		/** Model position in camera space/ world space */
		Mat4 m_Model;
	};
} }