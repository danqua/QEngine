#pragma once

#include <string>

namespace QEngine { namespace Renderer {

	class BaseWindow
	{
	public:
		/**
		 * Default destructor
		 */
		virtual ~BaseWindow(void) = default;

		/**
		 * Init
		 *
		 * Initializes the window.
		 *
		 * @param sTitle Window title
		 * @param width Window width
		 * @param height Window height
		 * @param eApi The graphics api, default OpenGL
		 */
		virtual void Init(const std::string& sTitle, int iWidth, int iHeight) = 0;
	
		/**
		 * Terminate
		 *
		 * Closes the window and destroys the rendering context
		 */
		virtual void Terminate(void) = 0;

		/**
		 * SwapBuffers
		 *
		 * Swaps the frame buffers
		 */
		virtual void SwapBuffers(void) = 0;

		/**
		 * IsRunning
		 *
		 * @return Returns true if the window is currently running, otherwise false
		 */
		inline bool IsRunning(void) const { return m_bRunning; }

		/**
		* Width
		*
		* @return Returns the width of the window
		*/
		inline const int& GetWidth(void) const { return m_Width; }

		/**
		* Height
		*
		* @return Returns the height of the window
		*/
		inline const int& GetHeight(void) const { return m_Height; }

		/**
		 * GetHandle
		 *
		 * @return Returns the window handle
		 */
		void* GetHandle(void) { return m_pWindowHandle; }

	protected:
		/** Window handle */
		void*	m_pWindowHandle;
		/** Window width */
		int		m_Width;
		/** Window height */
		int		m_Height;
		/** Flag */
		bool	m_bRunning;
	};
} }