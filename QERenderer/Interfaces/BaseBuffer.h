#pragma once

namespace QEngine { namespace Renderer {

	class BaseBuffer
	{
	public:
		/**
		* Default virtual destructor
		*/
		virtual ~BaseBuffer(void) = default;

		/**
		 * Binds the buffer object.
		 */
		virtual void Bind(void) const = 0;

		/**
		 * Unbinds the buffer object
		 */
		virtual void Unbind(void) const = 0;

	protected:
		/** Vertex buffer object id */
		unsigned int m_BufferId;
	};
} }