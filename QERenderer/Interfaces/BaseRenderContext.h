#pragma once

namespace QEngine { namespace Renderer {

	class BaseWindow;

	class BaseRenderContext
	{
	protected:
		BaseWindow*		m_pWindow;
		void*			m_pRenderContext;
	public:
		// Destructor
		virtual ~BaseRenderContext(void) = default;
		// Initialize
		virtual void Init(BaseWindow* pRenderDevice) = 0;
	};
} }