#pragma once

#include "BaseBuffer.h"

namespace QEngine { namespace Renderer {

	class BaseIndexBuffer : public BaseBuffer
	{
	public:
		/**
		* Default virtual destructor
		*/
		virtual ~BaseIndexBuffer(void) = default;

		/**
		 * Init
		 *
		 * Initializes the vertex buffer object.
		 *
		 * @param data Vertex data
		 * @param size The size in bytes
		 * @param numComponents The number of components per vertex
		 */
		virtual void Init(unsigned short* data, int count) = 0;
		
		/**
		 * GetCount
		 *
		 * @return Number of elements
		 */
		unsigned int GetCount(void) const { return m_NumElements; }

	protected:
		/** Number of components per vertex */
		unsigned int m_NumElements;
	};
} }