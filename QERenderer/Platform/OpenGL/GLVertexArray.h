#pragma once

#include "Interfaces/BaseVertexArray.h"

namespace QEngine { namespace Renderer {

	class GLVertexArray : public BaseVertexArray
	{
	public:
		GLVertexArray(void);
		/**
		* Default destructor
		*/
		~GLVertexArray(void);

		/**
		 * Init
		 *
		 * Initializes the vertex buffer object.
		 *
		 * @param data Vertex data
		 * @param size The size in bytes
		 * @param numComponents The number of components per vertex
		 */
		virtual void Init(void) override;
		
		/**
		 * AddBuffer
		 *
		 * Adds a vertex buffer to this vertex array object.
		 *
		 * @param BaseVertexBuffer Interface to the vertex buffer object
		 */
		virtual void AddVertexBuffer(BaseVertexBuffer* pVertexBuffer, unsigned int index) override;
		/**
		 * Binds the vertex array buffer object.
		 */
		virtual void Bind(void) const override;

		/**
		 * Unbinds the vertex array buffer object.
		 */
		virtual void Unbind(void) const override;
	};
} }