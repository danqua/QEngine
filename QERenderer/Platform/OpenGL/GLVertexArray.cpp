#include "GLVertexArray.h"
#include <GL/glew.h>

namespace QEngine { namespace Renderer {

	GLVertexArray::GLVertexArray(void)
	{
		glGenVertexArrays(1, &m_BufferId);
	}
	GLVertexArray::~GLVertexArray(void)
	{
		//for (auto* vbo : m_VertexBuffers)
		//	delete vbo;
	}

	void GLVertexArray::Init(void)
	{
		// glGenVertexArrays(1, &m_BufferId);
	}

	void GLVertexArray::AddVertexBuffer(BaseVertexBuffer* pVertexBuffer, unsigned int index)
	{
		Bind(); // Bind itself

		pVertexBuffer->Bind();

		glEnableVertexAttribArray(index);
		glVertexAttribPointer(index, pVertexBuffer->GetComponentCount(), GL_FLOAT, GL_FALSE, 0, (void*)0);
		//glDisableVertexAttribArray(index);

		pVertexBuffer->Unbind();

		Unbind(); // Unbind itself

		m_VertexBuffers.push_back(pVertexBuffer);
	}

	void GLVertexArray::Bind(void) const
	{
		glBindVertexArray(m_BufferId);
	}

	void GLVertexArray::Unbind(void) const
	{
		glBindVertexArray(0);
	}

} }