#include "GLTexture.h"

#include "Util/Image.h"
#include <GL/glew.h>

namespace QEngine { namespace Renderer { 

	GLTexture::GLTexture(void)
	{
		glGenTextures(1, &m_TextureId);
	}

	GLTexture::~GLTexture(void)
	{
		glDeleteTextures(1, &m_TextureId);
	}

	void GLTexture::LoadFromFile(const std::string& sFilename)
	{
		//int width, height, numComponents;
		//unsigned char* data = stbi_load(sFilename.c_str(), &width, &height, &numComponents, 4);
		Image image;
		image.LoadFromFile(sFilename);
		
		glBindTexture(GL_TEXTURE_2D, m_TextureId);
		//glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image.GetWidth(), image.GetHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, image.GetData());
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		glBindTexture(GL_TEXTURE_2D, 0);
	}

	void GLTexture::Bind(unsigned int slot) const
	{
		glActiveTexture(GL_TEXTURE0 + slot);
		glBindTexture(GL_TEXTURE_2D, m_TextureId);
	}

	void GLTexture::Unbind(void) const
	{
		glBindTexture(GL_TEXTURE_2D, 0);
	}

} }