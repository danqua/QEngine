#include "GLVertexBuffer.h"

#include <GL/glew.h>

namespace QEngine { namespace Renderer {

	void GLVertexBuffer::Init(void* data, int size, unsigned int numComponents)
	{
		glGenBuffers(1, &m_BufferId);
		glBindBuffer(GL_ARRAY_BUFFER, m_BufferId);
		glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		
		m_NumComponents = numComponents;
	}

	void GLVertexBuffer::Bind(void) const
	{
		glBindBuffer(GL_ARRAY_BUFFER, m_BufferId);
	}

	void GLVertexBuffer::Unbind(void) const
	{
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}

} }