#pragma once

#include "Interfaces/BaseRenderer2D.h"

namespace QEngine { namespace Renderer {

	class GLRenderer2D : public BaseRenderer2D
	{
	public:
		virtual void Draw(BaseRenderable2D* pObject) override;
	};
} }