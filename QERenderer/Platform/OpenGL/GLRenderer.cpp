#include "GLRenderer2D.h"

#include <GL/glew.h>

namespace QEngine { namespace Renderer {

	void GLRenderer2D::Draw(BaseRenderable2D* pObject)
	{
		pObject->BindVertexArray();
		pObject->BindIndexBuffer();

		glDrawElements(GL_TRIANGLES, pObject->GetIndexBufferCount(), GL_UNSIGNED_SHORT, 0);

		pObject->UnbindIndexBuffer();
		pObject->UnbindVertexArray();
	}
} }