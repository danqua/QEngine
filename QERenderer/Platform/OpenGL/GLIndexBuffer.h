#pragma once

#include "Interfaces/BaseIndexBuffer.h"

namespace QEngine { namespace Renderer {

	class GLIndexBuffer : public BaseIndexBuffer
	{
	public:
		GLIndexBuffer(void) { }
		~GLIndexBuffer() = default;

		virtual void Init(unsigned short* data, int count) override;
		virtual void Bind(void) const override;
		virtual void Unbind(void) const override;
	};
} }