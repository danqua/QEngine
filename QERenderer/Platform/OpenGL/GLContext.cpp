#include "GLContext.h"

#include "Interfaces/BaseWindow.h"

#include <string>
#include <SDL.h>
#include <GL/glew.h>

namespace QEngine { namespace Renderer {

	GLContext::GLContext(void)
	{
		m_pWindow			= nullptr;
		m_pRenderContext	= nullptr;
	}

	GLContext::~GLContext(void)
	{
		SDL_GL_DeleteContext(*((SDL_GLContext*)m_pRenderContext));
	}

	void GLContext::Init(BaseWindow* pWindow)
	{
		m_pWindow = pWindow;
		
		SetGLAttributes();
		SDL_GLContext glContext = SDL_GL_CreateContext((SDL_Window*)m_pWindow->GetHandle());

		// Point to the OpenGL context
		m_pRenderContext = new SDL_GLContext;
		m_pRenderContext = &glContext;

		if (!glContext)
			throw std::runtime_error("[GLContext] Failed to create OpenGL context!");

		glewExperimental = true;
		if (glewInit() != GLEW_OK)
			throw std::runtime_error("[GLContext] Failed to initialize GLEW!");

		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		SDL_GL_SetSwapInterval(0);

		glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	}

	void GLContext::SetGLAttributes(void)
	{
		SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

		SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 16);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
		SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 16);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
		SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8);
		SDL_GL_SetAttribute(SDL_GL_ACCELERATED_VISUAL, 1);
	}

} }