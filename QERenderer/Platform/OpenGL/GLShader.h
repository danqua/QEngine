#pragma once

#include "Interfaces/BaseShader.h"


namespace QEngine { namespace Renderer {

	typedef unsigned int GLuint;
	typedef unsigned int GLenum;
		
	class GLShader : public BaseShader
	{
	public:
		GLShader(void);
		~GLShader(void);
		virtual void Load(const std::string& sVertexShaderFilename, const std::string& sFragmentShaderFilename) override;
		virtual void Bind(void) const override;
		virtual void Unbind(void) const override;
		virtual void AddUniformLocation(const std::string& sName) override;
		virtual void SetUniformLocationMat4(const std::string& sName, const Mat4& data) override;
		virtual void SetUniformLocationVec3(const std::string& sName, const Vec3& data) override;
		virtual void SetUniformLocationVec2(const std::string& sName, const Vec2& data) override;
		virtual void SetUniformLocationFloat(const std::string& sName, const float& data) override;
		virtual void SetUniformLocationInteger(const std::string& sName, const int& data) override;
	private:
		void CompileShader(GLuint shader, const std::string& source);
		bool CheckErrors(GLuint shader, GLenum type);
		std::string LoadShaderSource(const std::string& sFilepath);
	};

} }