#pragma once

#include "Interfaces/BaseVertexBuffer.h"

namespace QEngine { namespace Renderer {

	class GLVertexBuffer : public BaseVertexBuffer
	{
	public:
		GLVertexBuffer(void) { }
		~GLVertexBuffer(void) = default;
		virtual void Init(void* data, int size, unsigned int numComponents) override;
		virtual void Bind(void) const override;
		virtual void Unbind(void) const override;
	};
} }