#include "GLIndexBuffer.h"

#include <GL/glew.h>

namespace QEngine { namespace Renderer {


	void GLIndexBuffer::Init(unsigned short* data, int count)
	{
		glGenBuffers(1, &m_BufferId);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferId);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, count * sizeof(GLushort), data, GL_STATIC_DRAW);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		
		m_NumElements = count;
	}

	void GLIndexBuffer::Bind(void) const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_BufferId);
	}

	void GLIndexBuffer::Unbind(void) const
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	}

} }