#pragma once

#include "Interfaces/BaseTexture.h"

namespace QEngine { namespace Renderer { 

	class GLTexture : public BaseTexture
	{
	public:
		GLTexture(void);
		~GLTexture(void);
		virtual void LoadFromFile(const std::string& filename) override;
		virtual void Bind(unsigned int slot = 0) const override;
		virtual void Unbind(void) const override;
	};

} }

