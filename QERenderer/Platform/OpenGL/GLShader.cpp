#include "GLShader.h"


#include <GL/glew.h>
#include <glm/glm.hpp>
#include <fstream>
#include <sstream>
#include <vector>

namespace QEngine { namespace Renderer {

	GLShader::GLShader(void)
	{
		// Creating the program
		m_Program = glCreateProgram();

		// Check if the program creation failed
		if (m_Program < 1)
			throw std::runtime_error("Could not create the shader program!");
	}

	GLShader::~GLShader(void)
	{
		// Delete the shader program
		if (glIsProgram(m_Program))
			glDeleteProgram(m_Program);
	}

	void GLShader::Load(const std::string& sVertexShaderPath, const std::string& sFragmentShaderPath)
	{
		
		// Load shader from file
		std::string vertexSource	= LoadShaderSource(sVertexShaderPath);
		std::string fragmentSource	= LoadShaderSource(sFragmentShaderPath);

		// Create shader
		GLuint vertexShader = glCreateShader(GL_VERTEX_SHADER);
		GLuint fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);

		// Check if shader creation failed
		if (!vertexShader || !fragmentShader)
		{
			throw std::runtime_error("[GLShader] Failed to create shader.");
		}

		// Compile shaders
		CompileShader(vertexShader, vertexSource);
		CompileShader(fragmentShader, fragmentSource);

		// Check for any errors
		CheckErrors(vertexShader, GL_SHADER);
		CheckErrors(fragmentShader, GL_SHADER);

		// Attach shaders to program
		glAttachShader(m_Program, vertexShader);
		glAttachShader(m_Program, fragmentShader);

		// Link the program
		glLinkProgram(m_Program);

		CheckErrors(m_Program, GL_PROGRAM);

		// After linking we don't need the intermediate shader objetcts anymore
		glDetachShader(m_Program, vertexShader);
		glDetachShader(m_Program, fragmentShader);

		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
	}

	void GLShader::Bind(void) const
	{
		// Check if shader program is valid
		if (glIsProgram(m_Program))
			glUseProgram(m_Program);
	}

	void GLShader::Unbind(void) const
	{
		glUseProgram(0);
	}

	void GLShader::AddUniformLocation(const std::string& sName)
	{
		m_UniformLocations[sName] = glGetUniformLocation(m_Program, sName.c_str());
	}

	std::string GLShader::LoadShaderSource(const std::string& sFilepath)
	{
		// Open file
		std::ifstream file(sFilepath, std::ios::in);
		std::stringstream sstream;
		// Check if something went wrong
		if (!file.is_open())
			throw std::runtime_error("[GLShader] Could not load file " + sFilepath);

		// Read file buffer into string stream
		sstream << file.rdbuf();
		// Close file
		file.close();
		// Return contents of file
		return sstream.str();
	}

	void GLShader::CompileShader(GLuint shader, const std::string& source)
	{
		const char* sc = source.c_str();
		glShaderSource(shader, 1, &sc, NULL);
		glCompileShader(shader);
	}

	bool GLShader::CheckErrors(GLuint shader, GLenum type)
	{
		// Check for errors
		GLint result = GL_FALSE;
		int infoLength;

		if (type == GL_SHADER)
		{
			glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
			glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infoLength);
		}
		else if (type == GL_PROGRAM)
		{
			glGetProgramiv(shader, GL_LINK_STATUS, &result);
			glGetProgramiv(shader, GL_INFO_LOG_LENGTH, &infoLength);

		}
		if (infoLength > 0)
		{
			std::vector<char> errorMessage(infoLength + 1);

			if (type == GL_SHADER)
				glGetShaderInfoLog(shader, infoLength, NULL, &errorMessage[0]);
			else if (type == GL_PROGRAM)
				glGetProgramInfoLog(shader, infoLength, NULL, &errorMessage[0]);
#ifdef _DEBUG
			printf("%s\n", &errorMessage[0]);
#endif
			return false;
		}
		return true;
	}

	// Mat4x4
	void GLShader::SetUniformLocationMat4(const std::string& sName, const Mat4& data)
	{
		glUniformMatrix4fv(m_UniformLocations[sName], 1, GL_FALSE, &data[0][0]);
	}

	void GLShader::SetUniformLocationVec3(const std::string& sName, const Vec3& data)
	{
		glUniform3fv(m_UniformLocations[sName], 1, &data[0]);
	}

	void GLShader::SetUniformLocationVec2(const std::string& sName, const Vec2& data)
	{
		glUniform2fv(m_UniformLocations[sName], 1, &data[0]);
	}

	void GLShader::SetUniformLocationFloat(const std::string& sName, const float& data)
	{
		glUniform1f(m_UniformLocations[sName], data);
	}

	void GLShader::SetUniformLocationInteger(const std::string& sName, const int& data)
	{
		glUniform1i(m_UniformLocations[sName], data);
	}
} }
