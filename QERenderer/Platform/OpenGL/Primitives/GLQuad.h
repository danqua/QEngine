#pragma once

#include "Interfaces/BaseRenderable2D.h"
#include "Math/Vector.h"
#include "Common/Transform.h"

namespace QEngine { namespace Renderer {

	class GLQuad : public BaseRenderable2D
	{
	public:
		/**
		 * transform
		 *
		 * Expose transformation to public.
		 */
		Transform transform;

		/**
		 * Constructor
		 */
		GLQuad(void);
		
		/**
		 * Destructor
		 */
		virtual ~GLQuad(void);

		/**
		 * Update
		 */
		virtual void Update(float fDelta = 1.0f) override;
	};
} }