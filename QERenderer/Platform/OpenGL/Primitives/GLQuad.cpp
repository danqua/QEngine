#pragma once

#include "GLQuad.h"
#include "Platform/OpenGL/GLShader.h"
#include "Platform/OpenGL/GLVertexArray.h"
#include "Platform/OpenGL/GLIndexBuffer.h"
#include "Platform/OpenGL/GLVertexBuffer.h"

#include <GL/glew.h>

namespace QEngine { namespace Renderer {

	GLQuad::GLQuad(void)
	{
		// Vertices
		GLfloat vertices[] =
		{
			-0.5f, -0.5f, 0.0f,	// bottom left
			 0.5f, -0.5f, 0.0f,	// bottom right
			 0.5f,  0.5f, 0.0f,	// top right
			-0.5f,  0.5f, 0.0f	// top left
		};

		// Texture uv
		GLfloat uvs[] =
		{
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f
		};

		// Indices
		GLushort indices[] =
		{
			0, 1, 2,
			2, 3, 0
		};

		m_pVertexArray = new GLVertexArray;
		m_pIndexBuffer = new GLIndexBuffer;
		
		GLVertexBuffer vbo;
		GLVertexBuffer uvo;
		vbo.Init(vertices, sizeof(vertices), 3);
		uvo.Init(uvs, sizeof(uvs), 2);

		m_pIndexBuffer->Init(indices, 6);

		// Add vertex buffers to array
		m_pVertexArray->AddVertexBuffer(&vbo, SHADER_VERTEX_ATTRIBUTE);
		m_pVertexArray->AddVertexBuffer(&uvo, SHADER_UV0_ATTRIBUTE);
	}

	GLQuad::~GLQuad(void)
	{

	}

	void GLQuad::Update(float fDelta)
	{
		if (transform.rotation.x * fDelta > 360.0f)
			transform.rotation.x -= 360.0f;
		if (transform.rotation.y * fDelta > 360.0f)
			transform.rotation.y -= 360.0f;
		if (transform.rotation.z * fDelta > 360.0f)
			transform.rotation.z -= 360.0f;

		// Move back to original position
		m_Model = glm::translate(Mat4(1.0f), transform.position);
		// Rotate around x axis
		m_Model = glm::rotate(m_Model, glm::radians(transform.rotation.x), Vec3(1.0f, 0.0f, 0.0f));
		// Rotate around y axis
		m_Model = glm::rotate(m_Model, glm::radians(transform.rotation.y), Vec3(0.0f, 1.0f, 0.0f));
		// Rotate around z axis
		m_Model = glm::rotate(m_Model, glm::radians(transform.rotation.z * fDelta), Vec3(0.0f, 0.0f, 1.0f));
		// Scale this bad motherlover
		m_Model = glm::scale(m_Model, transform.scale);
	}
} }