#pragma once

#include "Interfaces/BaseRenderContext.h"

namespace QEngine { namespace Renderer {

	class GLContext : public BaseRenderContext
	{
	public:
		// Constructor
		GLContext(void);
		// Destructor
		~GLContext(void);
		// Initialize
		virtual void Init(BaseWindow* pWindow) override;
	private:
		// Setup GL Attributs
		void SetGLAttributes(void);
	};
} }