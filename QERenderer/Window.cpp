#include "Window.h"

#include <SDL.h>
#ifdef _WIN32
	#undef main
#endif

namespace QEngine { namespace Renderer {

	Window::Window(void)
	{
		// Initialize SDL
		if (SDL_Init(SDL_INIT_VIDEO) < 0)
			throw std::runtime_error("[Window] Failed to initialize SDL.");

		// Set default values for properties
		m_pWindowHandle = nullptr;
		m_bRunning	= false;
		m_Width		= 0;
		m_Height	= 0;
	}

	Window::~Window()
	{
		if (m_pWindowHandle)
			SDL_DestroyWindow((SDL_Window*)m_pWindowHandle);

		SDL_Quit();

		m_pWindowHandle = nullptr;
	}

	void Window::Init(const std::string& sTitle, int width, int height)
	{
		// Creating the window
		m_pWindowHandle = SDL_CreateWindow(
			sTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);

		if (!m_pWindowHandle)
			throw std::runtime_error("[Window] Failed to create the window.");

		m_bRunning	= true;
		m_Width		= width;
		m_Height	= height;
	}

	void Window::Terminate(void)
	{
		m_bRunning = false;
	}

	bool Window::IsRunning(void)
	{
		return m_bRunning;
	}

	void Window::SwapBuffers(void)
	{
		SDL_GL_SwapWindow((SDL_Window*)m_pWindowHandle);
	}

} }