#pragma once

#include <vector>
#include "Common/BspMap.h"
#include "Q3BspInfo.h"
#include "Q3BspRenderHelpers.h"
#include "Q3BspPatch.h"

#include "Renderer/GLTexture.h"

struct q3patch;
class Q3BspMap : virtual public BspMap
{
public:
	static const int sTesselationLevel;
	Q3BspMap(void) {}
	virtual ~Q3BspMap(void);

	void Init(void);
	void OnRenderStart(void);
	void Render(void);
	void OnRenderFinish(void);

	// bsp data
	q3header					header;
	q3entity					entities;
	std::vector<q3texture>		textures;
	std::vector<q3plane>		planes;
	std::vector<q3node>			nodes;
	std::vector<q3leafs>		leafs;
	std::vector<q3leafface>		leafFaces;
	std::vector<q3leafbrush>	leafBrushes;
	std::vector<q3model>		models;
	std::vector<q3brush>		brushes;
	std::vector<q3brushsides>	brushSides;
	std::vector<q3vertex>		vertices;
	std::vector<q3meshvert>		meshVertices;
	std::vector<q3effect>		effects;
	std::vector<q3face>			faces;
	std::vector<q3lightmap>		lightMaps;
	std::vector<q3lightvol>		lightVolumes;
	q3visdata					visData;

private:
	void LoadTextures(void);
	
	void CreateBuffersForFace(const q3face& face, int index);
	void CreateBuffersForPatch(int index);

	void CreatePatch(const q3face& face);
	void RenderFace(int index);
	void RenderPatch(int index);

	std::vector<q3facerenderable>	m_renderFaces;

	std::vector<q3patch*>			m_patches;
	std::vector<GLTexture*>			m_textures;
	q3renderbuf						m_renderBuffers;

};