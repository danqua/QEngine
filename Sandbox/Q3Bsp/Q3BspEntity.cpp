#include "Q3BspEntity.h"


Q3BspEntityParser::Q3BspEntityParser(void)
{

}

Q3BspEntityParser::~Q3BspEntityParser(void)
{

}

Q3BspEntities* Q3BspEntityParser::Parse(const q3entity& e)
{
	Q3BspEntities* result = new Q3BspEntities;
	// String for convinience
	std::string entities = std::string(e.string, e.size);
	std::stringstream sstream;
	sstream << entities;
	std::string line;

	std::map<std::string, std::string> entity;
	while (std::getline(sstream, line))
	{
		if (line == "{")
		{
			// First parse entites
			ParseEntity(sstream, entity);
			// Parse pairs build up by parsing the entities
			result->ParseMap(entity);
		}
	}
	return result;
}

void Q3BspEntityParser::ParseEntity(std::stringstream& sstream, std::map<std::string, std::string>& entity)
{
	std::string line;

	// The entries in the entity arent in order
	while (std::getline(sstream, line))
	{
		// Step out
		if (line[0] == '}') break;

		// Entities are split by space
		// Seek first space
		int pos = (int)line.find(" ");
		std::string key = line.substr(0, pos);
		std::string value = line.substr(pos + 1, line.length() - 1);

		// Get rid of quotes
		key		= key.substr(1, key.length() - 2);
		value	= value.substr(1, value.length() - 2);

		entity[key] = value;
	}
	
}

Q3BspEntities::Q3BspEntities(void)
{

}

Q3BspEntities::~Q3BspEntities(void)
{

}


void Q3BspEntities::ParseMap(std::map<std::string, std::string>& entity)
{
	if (!entity.count("classname"))
		return;

	//
	// ==== WORLDSPAWN ====
	//
	if (entity["classname"] == "worldspawn")
	{
		if (entity.count("message"))
			worldSpawn.message = entity["message"];

		if (entity.count("music"))
			worldSpawn.music = entity["music"];

		if (entity.count("ambient"))
			worldSpawn.ambient = std::stof(entity["ambient"]);

		if (entity.count("_color"))
		{
			std::stringstream sstream;
			sstream << entity["_color"];
			sstream >> worldSpawn.color.r >> worldSpawn.color.g >> worldSpawn.color.b;
		}
	}

	//
	// ==== LIGHT ====
	//
	if (entity["classname"] == "light")
	{
		Q3BspLightEntity l;

		if (entity.count("light"))
			l.intensity = std::stof(entity["light"]);

		if (entity.count("radius"))
			l.radius = std::stof(entity["radius"]);

		if (entity.count("_color"))
		{
			std::stringstream sstream;
			sstream << entity["_color"];
			sstream >> l.color.r >> l.color.g >> l.color.b;
		}

		if (entity.count("origin"))
		{
			std::stringstream sstream;
			sstream << entity["origin"];
			sstream >> l.position.x >> l.position.y >> l.position.z;
		}
		// Add to global lights array
		lights.push_back(l);
	}

	//
	// ==== PLAYER SPAWN ====
	//
	if (entity["classname"] == "info_player_deathmatch")
	{
		Q3BspInfoPlayerEntity player;

		if (entity.count("origin"))
		{
			std::stringstream sstream;
			sstream << entity["origin"];
			sstream >> player.position.x >> player.position.y >> player.position.z;
		}
		if (entity.count("angle"))
		{
			player.angle = std::stof(entity["angle"]);
		}
		playerSpawns.push_back(player);
	}

	//
	// ==== SPEAKER ====
	//
	if (entity["classname"] == "target_speaker")
	{
		Q3BspSpeakerEntity speaker;

		if (entity.count("origin"))
		{
			std::stringstream sstream;
			sstream << entity["origin"];
			sstream >> speaker.position.x >> speaker.position.y >> speaker.position.z;
		}

		if (entity.count("noise"))
		{
			speaker.soundFile = entity["noise"];
		}
		speakers.push_back(speaker);
	}
}

