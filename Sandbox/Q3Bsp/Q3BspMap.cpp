#include "Q3BspMap.h"

const int Q3BspMap::sTesselationLevel = 10;	// level of curved surface tesselation

Q3BspMap::~Q3BspMap(void)
{
	delete[] entities.string;
	delete[] visData.vecs;
	
	for (auto& it : m_renderBuffers.faceVBOs)
	{
		if (glIsBuffer(it.second.vertexBuffer))
		{
			glDeleteBuffers(1, &(it.second.vertexBuffer));
		}

		if (glIsBuffer(it.second.textureBuffer))
		{
			glDeleteBuffers(1, &it.second.textureBuffer);
		}
	}

	if (glIsVertexArray(m_renderBuffers.vao))
		glDeleteVertexArrays(1, &m_renderBuffers.vao);
}

void Q3BspMap::Init(void)
{
	// Load textures
	LoadTextures();

	// Load lightmaps
	// LoadLightmaps();
	m_renderFaces.reserve(faces.size());

	// Create VAO
	glGenVertexArrays(1, &m_renderBuffers.vao);
	glBindVertexArray(m_renderBuffers.vao);

	int faceArrayIdx = 0;
	int patchArrayIdx = 0;

	for (const auto& face : faces)
	{
		m_renderFaces.push_back(q3facerenderable());

		// If it's a patch = 2
		if (face.type == 2)
		{
			m_renderFaces.back().index = patchArrayIdx;
			CreatePatch(face);

			// Generate necessary VBOs for current patch
			CreateBuffersForPatch(patchArrayIdx);
			++patchArrayIdx;
		}
		else
		{
			m_renderFaces.back().index = faceArrayIdx;

			// Generate necessary VBOs for current face
			CreateBuffersForFace(face, faceArrayIdx);
		}
		++faceArrayIdx;
		m_renderFaces.back().type = face.type;
	}

	m_mapStats.totalVertices	= (int)vertices.size();
	m_mapStats.totalFaces		= (int)faces.size();
	m_mapStats.totalPatches		= (int)patchArrayIdx;

	printf("==== BSP MAP INFO ====\n");
	printf("Vertices: %d\n", m_mapStats.totalVertices);
	printf("Faces   : %d\n", m_mapStats.totalFaces);
	printf("Patches : %d\n", m_mapStats.totalPatches);
}

void Q3BspMap::OnRenderStart(void)
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_FRONT);
}

void Q3BspMap::Render(void)
{
	glBindVertexArray(m_renderBuffers.vao);
	
	glEnableVertexAttribArray(0);	// Position
	glEnableVertexAttribArray(1);	// UVs

	// For loop -> should be visible faces
	for (const auto& face : m_renderFaces)
	{
		// Patch
		if (face.type == 2)
		{
			RenderPatch(face.index);
			m_mapStats.visiblePatches++;
		}
		else
		{
			// Polygons
			RenderFace(face.index);
		}
	}

	glDisableVertexAttribArray(0);	// Position
	glDisableVertexAttribArray(1);	// UVs
}

void Q3BspMap::OnRenderFinish(void)
{
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
}

void Q3BspMap::LoadTextures(void)
{
	int numTextures = header.direntries[TEXTURES].length / sizeof(q3texture);
	m_textures.resize(numTextures);

	for (const auto& face : faces)
	{
		if (m_textures[face.texture])
			continue;
		
		std::string textureName = textures[face.texture].name;
		textureName.append(".jpg");
		m_textures[face.texture] = g_TextureManager->LoadTexture(textureName);

		if (m_textures[face.texture] == NULL)
		{
			// Try tga instead of jpg
			textureName = textures[face.texture].name;
			textureName.append(".tga");

			m_textures[face.texture] = g_TextureManager->LoadTexture(textureName);

			if (m_textures[face.texture] == NULL)
			{
				// Load default texture
				m_textures[face.texture] = g_TextureManager->LoadTexture("textures/default.jpg");
#ifdef _DEBUG
				printf("[Q3BspMap] Missing texture: %s\n", textures[face.texture].name);
#endif
			}
		}
	}
}

void Q3BspMap::CreateBuffersForFace(const q3face& face, int index)
{
	// Vertices
	glGenBuffers(1, &m_renderBuffers.faceVBOs[index].vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.faceVBOs[index].vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(q3vertex) * face.n_vertices, &(vertices[face.vertex].position), GL_STATIC_DRAW);

	// Should I do a normal buffer for custom lighting?

	// UVs
	glGenBuffers(1, &m_renderBuffers.faceVBOs[index].textureBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.faceVBOs[index].textureBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(q3vertex) * face.n_vertices, &vertices[face.vertex].texcoord[0], GL_STATIC_DRAW);

	// Lightmaps
}

void Q3BspMap::CreateBuffersForPatch(int index)
{
	int numPatches = (int)m_patches[index]->quadraticPatches.size();

	for (int i = 0; i < numPatches; ++i)
	{
		m_renderBuffers.patchVBOs[index].push_back(q3facebuf());

		int numVerts = (int)m_patches[index]->quadraticPatches[i].vertices.size();

		glGenBuffers(1, &m_renderBuffers.patchVBOs[index][i].vertexBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.patchVBOs[index][i].vertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(q3vertex) * numVerts, &m_patches[index]->quadraticPatches[i].vertices[0].position, GL_STATIC_DRAW);

		glGenBuffers(1, &m_renderBuffers.patchVBOs[index][i].textureBuffer);
		glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.patchVBOs[index][i].textureBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(q3vertex) * numVerts, &m_patches[index]->quadraticPatches[i].vertices[0].texcoord, GL_STATIC_DRAW);

		// Lightmap buffer
	}
}

void Q3BspMap::CreatePatch(const q3face& face)
{
	q3patch* patch = new q3patch;

	patch->textureIdx = face.texture;
	patch->lightmapIdx = face.lm_index;
	patch->width		= face.size.x;
	patch->height		= face.size.y;

	int numPatchesWidth = (patch->width - 1) >> 1;
	int numPatchesHeight = (patch->height - 1) >> 1;

	patch->quadraticPatches.resize(numPatchesWidth * numPatchesHeight);

	// generate biquadratic patches (components that make the curved surface)
	for (int y = 0; y < numPatchesHeight; ++y)
	{
		for (int x = 0; x < numPatchesWidth; ++x)
		{
			for (int row = 0; row < 3; ++row)
			{
				for (int col = 0; col < 3; ++col)
				{
					int patchIdx = y * numPatchesWidth + x;
					int cpIndex = row * 3 + col;
					patch->quadraticPatches[patchIdx].controlPoints[cpIndex] = vertices[face.vertex +
						(y * 2 * patch->width + x * 2) +
						(row * patch->width + col)];
				}
			}
			patch->quadraticPatches[y * numPatchesWidth + x].Tesselate(Q3BspMap::sTesselationLevel);
		}
	}
	m_patches.push_back(patch);
}

void Q3BspMap::RenderFace(int index)
{
	// Vertices
	glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.faceVBOs[index].vertexBuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(q3vertex), (void*)0);

	// Textures
	glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.faceVBOs[index].textureBuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(q3vertex), (void*)0);

	g_TextureManager->BindTexture(m_textures[faces[index].texture]);

	glDrawElements(GL_TRIANGLES, faces[index].n_meshverts, GL_UNSIGNED_INT, &meshVertices[faces[index].meshvert]);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void Q3BspMap::RenderPatch(int index)
{
	// Bind texture
	g_TextureManager->BindTexture(m_textures[m_patches[index]->textureIdx]);

	int numPatches = (int)m_patches[index]->quadraticPatches.size();

	for (int i = 0; i < numPatches; ++i)
	{
		// Bind vertices
		glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.patchVBOs[index][i].vertexBuffer);
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(q3vertex), (void*)0);

		// Bind texture coords
		glBindBuffer(GL_ARRAY_BUFFER, m_renderBuffers.patchVBOs[index][i].textureBuffer);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(q3vertex), (void*)0);

		// Bind lightmap coords
		m_patches[index]->quadraticPatches[i].Render();
	}
}
