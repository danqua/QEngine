#pragma once

#include "Common/Common.h"
#include <GL/glew.h>


typedef struct
{
	GLuint vertexBuffer;			// Stores the vbo id
	GLuint indexBuffer;
	GLuint indexCount;
	GLuint textureBuffer;
} q3facebuf;

typedef struct
{
	int type;
	int index;
} q3facerenderable;

typedef struct
{
	GLuint vao;
	std::map<int, q3facebuf> faceVBOs;
	std::map<int, std::vector<q3facebuf>> patchVBOs;
} q3renderbuf;