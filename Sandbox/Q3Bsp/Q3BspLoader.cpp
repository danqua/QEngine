#include "Q3BspLoader.h"
#include <fstream>

static std::string sRootDir = "C:/Users/danqu/CloudStation/Devel/QEngine/Assets/";
Q3BspMap* Q3BspLoader::Load(const std::string& filename)
{
	std::ifstream bspFile;
	bspFile.open(sRootDir + filename, std::ios::in | std::ios::binary);

	if (!bspFile.is_open())
	{
		throw std::runtime_error("[Q3BspLoader] Could not load bsp file: " + filename);
		return NULL;
	}

	// bsp header
	q3header bspHeader;
	LoadBspHeader(bspHeader, bspFile);

	// Validate the header
	bool validQ3Bsp = !strncmp(bspHeader.magic, "IBSP", 4) && (bspHeader.version == 0x2e);
	if (!validQ3Bsp)
	{
		throw std::runtime_error("[Q3BspLoader] Wrong bsp file version");
		return NULL;
	}

	// If we made it this far, the header is valid
	Q3BspMap* q3map = new Q3BspMap;
	q3map->header = bspHeader;

	// Entities lump
	LoadEntitiesLump(q3map, bspFile);

	LoadLump(q3map, TEXTURES,		q3map->textures,		bspFile);
	LoadLump(q3map, PLANES,			q3map->planes,			bspFile);
	LoadLump(q3map, NODES,			q3map->nodes,			bspFile);
	LoadLump(q3map, LEAFS,			q3map->leafs,			bspFile);
	LoadLump(q3map, LEAFFACES,		q3map->leafFaces,		bspFile);
	LoadLump(q3map, LEAFBRUSHES,	q3map->leafBrushes,		bspFile);
	LoadLump(q3map, MODELS,			q3map->models,			bspFile);
	LoadLump(q3map, BRUSHES,		q3map->brushes,			bspFile);
	LoadLump(q3map, BRUSHSIDES,		q3map->brushSides,		bspFile);
	LoadLump(q3map, VERTEXES,		q3map->vertices,		bspFile);
	LoadLump(q3map, MESHVERTS,		q3map->meshVertices,	bspFile);
	LoadLump(q3map, EFFECTS,		q3map->effects,			bspFile);
	LoadLump(q3map, FACES,			q3map->faces,			bspFile);
	LoadLump(q3map, LIGHTMAPS,		q3map->lightMaps,		bspFile);
	LoadLump(q3map, LIGHTVOLS,		q3map->lightVolumes,	bspFile);

	// Load vis data lump
	LoadVisDataLump(q3map, bspFile);
	return q3map;
}

void Q3BspLoader::LoadBspHeader(q3header& header, std::ifstream& fstream)
{
	fstream.read((char*)&header, sizeof(q3header));
}


void Q3BspLoader::LoadEntitiesLump(Q3BspMap* map, std::ifstream& fstream)
{
	map->entities.size		= map->header.direntries[ENTITIES].length;
	map->entities.string	= new char[map->entities.size];

	fstream.seekg(map->header.direntries[ENTITIES].offset, std::ios_base::beg);
	fstream.read(map->entities.string, sizeof(char) * map->entities.size);
}

void Q3BspLoader::LoadVisDataLump(Q3BspMap* map, std::ifstream& fstream)
{
	fstream.seekg(map->header.direntries[VISDATA].offset, std::ios_base::beg);
	fstream.read((char*)&map->visData.n_vecs, sizeof(int));
	fstream.read((char*)&map->visData.sz_vecs, sizeof(int));

	int vecSize = map->visData.n_vecs * map->visData.sz_vecs;
	map->visData.vecs = new unsigned char[vecSize];
	fstream.read((char*)map->visData.vecs, vecSize * sizeof(unsigned char));
}

template <typename T>
void Q3BspLoader::LoadLump(Q3BspMap* map, LUMP_TYPE type, std::vector<T>& container, std::ifstream& fstream)
{
	int numElements = map->header.direntries[type].length / sizeof(T);
	fstream.seekg(map->header.direntries[type].offset, std::ios_base::beg);

	container.reserve(numElements);

	for (int i = 0; i < numElements; ++i)
	{
		T element;
		fstream.read((char*)&element, sizeof(T));
		container.push_back(element);
	}
}
