#pragma once

typedef struct
{
	float x;
	float y;
} vec2f;

typedef struct  
{
	float x;
	float y;
	float z;
} vec3f;

typedef struct
{
	int x;
	int y;
} vec2i;

typedef struct
{
	int x;
	int y;
	int z;
} vec3i;


enum LUMP_TYPE
{
	ENTITIES	= 0,	// Game related object description
	TEXTURES	= 1,	// Surface descriptions
	PLANES		= 2,	// Planes used by map geometry
	NODES		= 3,	// BSP tree nodes
	LEAFS		= 4,	// BSP tree leafs
	LEAFFACES	= 5,	// List of face indices, one list per leaf
	LEAFBRUSHES = 6,	// List of brush indices, one list per leaf
	MODELS		= 7,	// Descriptions of rigid world geometry in map
	BRUSHES		= 8,	// Convex polyhedra used to describe solid space
	BRUSHSIDES	= 9,	// Brush surfaces
	VERTEXES	= 10,	// Vertices used to describe faces
	MESHVERTS	= 11,	// Lists of offsets, one list per mesh
	EFFECTS		= 12,	// List of special map effects
	FACES		= 13,	// Surface geometry
	LIGHTMAPS	= 14,	// Packed lightmap data
	LIGHTVOLS	= 15,	// Local illumination data
	VISDATA		= 16,	// Cluster-cluster visibility data
};
#define HEADER_LUMPS 17

typedef struct
{
	int offset;		// Offset to start of lumps, relative to beginning of file
	int length;		// Length of lump. Always a multiple of 4
} q3direntry;

typedef struct
{
	char		magic[4];					// Must be "IBSP"
	int			version;					// Must be 0x2e
	q3direntry	direntries[HEADER_LUMPS];	// Lump directory
} q3header;

typedef struct
{
	int size;		// Size
	char* string;	// Entity descriptions, stored as a string
} q3entity;

struct q3vertex
{
	vec3f	position;			// Vertex position
	vec2f	texcoord[2];		// Vertex texture coordinates. 0 = surface, 1 = lightmap
	vec3f	normal;				// Vertex normal
	unsigned char color[4];		// Vertex color. RGBA

	q3vertex operator+(const q3vertex& rhs) const
	{
		q3vertex result;
		result.position.x = position.x + rhs.position.x;
		result.position.y = position.y + rhs.position.y;
		result.position.z = position.z + rhs.position.z;
		
		result.texcoord[0].x = texcoord[0].x + rhs.texcoord[0].x;
		result.texcoord[0].y = texcoord[0].y + rhs.texcoord[0].y;
		result.texcoord[1].x = texcoord[1].x + rhs.texcoord[1].x;
		result.texcoord[1].y = texcoord[1].y + rhs.texcoord[1].y;

		return result;
	};

	q3vertex operator*(const float rhs) const
	{
		q3vertex result;
		result.position.x = position.x * rhs;
		result.position.y = position.y * rhs;
		result.position.z = position.z * rhs;
		result.texcoord[0].x = texcoord[0].x * rhs;
		result.texcoord[0].y = texcoord[0].y * rhs;
		result.texcoord[1].x = texcoord[1].x * rhs;
		result.texcoord[1].y = texcoord[1].y * rhs;

		return result;
	};

};

typedef struct
{
	int offset;	// Vertex index offset, relative to first vertex of corresponding face
} q3meshvert;

typedef struct
{
	int		texture;		// Texture index
	int		effect;			// Index into lump EFFECTS, or -1
	int		type;			// Face type: 1 = polygon, 2 = path, 3 = mesh, 4 = billboard
	int		vertex;			// Index of first vertex
	int		n_vertices;		// Number of vertices
	int		meshvert;		// Index of first meshvert
	int		n_meshverts;	// Number of meshverts
	int		lm_index;		// Lightmap index
	vec2i	lm_start;		// Corner of this face's lightmap image in lightmap
	vec2i	lm_size;		// Size of this face's lightmap image in lightmap
	vec3f	lm_origin;		// World space origin of lightmap
	vec3f	lm_vecs[2];		// World space lightmap s and t unit vectors
	vec3f	normal;			// Surface normal
	vec2i	size;			// Patch dimensions
} q3face;

typedef struct
{
	char	name[64];		// Texture name
	int		flags;			// Surface flag
	int		contents;		// Content flags
} q3texture;

typedef struct
{
	vec3f normal;			// Plane normal
	float dist;				// Distance from origin to plane along normal
} q3plane;

typedef struct
{
	int plane;			// Plane index
	vec2i children;		// Children indices. Negative numbers are leaf indices
	vec3i mins;			// Inetger bounding box min coords
	vec3i maxs;			// Integer bounding box max coords
} q3node;

typedef struct
{
	int cluster;		// Visdata cluster index
	int area;			// Areaportal area
	vec3i mins;			// Interger bounding box min coords
	vec3i maxs;			// Interger bounding box max coords
	int leafface;		// First leafface for leaf
	int n_leaffaces;	// Number of leaffaces for leaf
	int leafbrush;		// First leafbrush for leaf
	int n_leafbrushes;	// Number of leafbrushes for leaf
} q3leafs;

typedef struct
{
	int face;			// Face index
} q3leafface;

typedef struct
{
	int brush;			// Brush index
} q3leafbrush;

typedef struct
{
	vec3f mins;			// Bounding box min coord
	vec3f maxs;			// Bounding box min coord
	int face;			// First face for model
	int n_faces;		// Number of faces for model
	int brush;			// First brush for model
	int n_brushes;		// Number of brushes for model
} q3model;

typedef struct
{
	int brushside;		// First brushside for brush
	int n_brushsides;	// Number of brushsides for brush
	int texture;		// Texture index
} q3brush;

typedef struct
{
	int plane;			// Plane index
	int texture;		// Texture index
} q3brushsides;

typedef struct
{
	char name[64];		// Effect shader
	int brush;			// Brush taht generated this effect
	int unknown;		// ??
} q3effect;

typedef struct
{
	unsigned char map[128 * 128 * 3];	// Lightmap color data RGB
} q3lightmap;

typedef struct
{
	unsigned char ambient[3];		// Ambient color component. RGB
	unsigned char directional[3];	// Directional color component RGB
	unsigned char dir[2];			// Direction to light 0 = phi, 1 = theta
} q3lightvol;

typedef struct
{
	int n_vecs;				// Number of vectors
	int sz_vecs;			// Size of each vector in bytes
	unsigned char* vecs;	// Visibility data. One bit per cluster per vector
} q3visdata;