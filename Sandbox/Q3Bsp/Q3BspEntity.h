#pragma once

#include "Common/Precompiled.h"
#include "Q3BspInfo.h"

#include <sstream>

struct Q3BspLightEntity
{
	Q3BspLightEntity(void) :
		intensity(0.0f),
		radius(1.0f),
		color(glm::vec3(1.0f, 0.0f, 1.0f)),
		position(glm::vec3(0.0f)) {}

	float		intensity;	// Light intensity
	float		radius;		// Light radius
	glm::vec3	color;		// Light color RGB
	glm::vec3	position;	// Origin coordinates x y z
};

struct Q3BspSpeakerEntity
{
	Q3BspSpeakerEntity(void) :
		position(glm::vec3(0.0f)),
		soundFile("UNKNOWN") {}

	glm::vec3	position;
	std::string	soundFile;
};

struct Q3BspInfoPlayerEntity
{
	Q3BspInfoPlayerEntity(void) :
		position(glm::vec3(0.0f)),
		angle(0.0f) {}

	glm::vec3	position;	// Player spawn position
	float		angle;		// Rotation angle in degrees
};

struct Q3BspInfoWorldSpawnEntity
{
	Q3BspInfoWorldSpawnEntity(void) :
		message("UNKNOWN"),
		music("UNKNOWN"),
		color(glm::vec3(1.0, 0.0f, 1.0f)),
		ambient(0.0f) {};

	std::string message;
	std::string music;
	glm::vec3	color;
	float		ambient;
};

class Q3BspEntities
{
public:
	Q3BspEntities(void);
	virtual ~Q3BspEntities(void);


	void ParseMap(std::map<std::string, std::string>& entity);

	Q3BspInfoWorldSpawnEntity			worldSpawn;
	std::vector<Q3BspLightEntity>		lights;
	std::vector<Q3BspSpeakerEntity>		speakers;
	std::vector<Q3BspInfoPlayerEntity>	playerSpawns;
};

class Q3BspEntityParser
{
public:
	Q3BspEntityParser(void);
	virtual ~Q3BspEntityParser(void);
	Q3BspEntities* Parse(const q3entity& entities);
private:
	void ParseEntity(std::stringstream& sstream, std::map<std::string, std::string>& entity);
};