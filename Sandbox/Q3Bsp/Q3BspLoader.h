#pragma once

#include "Q3BspMap.h"

class Q3BspLoader
{
public:
	Q3BspMap* Load(const std::string& filename);

private:
	template <typename T>
	void LoadLump(Q3BspMap* map, LUMP_TYPE type, std::vector<T>& container, std::ifstream& fstream);
	void LoadBspHeader(q3header& header, std::ifstream& fstream);
	void LoadEntitiesLump(Q3BspMap* map, std::ifstream& fstream);
	void LoadVisDataLump(Q3BspMap* map, std::ifstream& fstream);
};