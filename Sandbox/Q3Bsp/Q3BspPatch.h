#pragma once

#include "Q3BspMap.h"

// Quake III BSP curved surface component (biqaudratic (3x3) patch)
class Q3BspBiquadPatch
{
public:
	Q3BspBiquadPatch() :
		m_tesselationLevel(0),
		m_trianglesPerRow(NULL),
		m_rowIndexPointers(NULL) {}
	
	virtual ~Q3BspBiquadPatch()
	{
		delete[] m_trianglesPerRow;
		delete[] m_rowIndexPointers;
	}
	
	void Tesselate(int tessLevel);
	void Render(void);

	q3vertex				controlPoints[9];
	std::vector<q3vertex>	vertices;
private:
	int							m_tesselationLevel;
	std::vector<unsigned int>	m_indices;
	int*						m_trianglesPerRow;	// Stored as pointer arrays for easer access by OpelGL
	unsigned int**				m_rowIndexPointers;
};

struct q3patch
{
	int textureIdx;		// Surface texture index
	int lightmapIdx;	// Surface lightmap index
	int width;
	int height;
	std::vector<Q3BspBiquadPatch> quadraticPatches;
};