#version 440 core

in vec2 _uv0;
in vec2 _uv1;
in vec2 _uv2;
in vec2 _uv3;
in vec3 _normal;

out vec4 FragColor;

uniform sampler2D tex0;
uniform sampler2D tex1;
uniform sampler2D tex2;

void main()
{
	FragColor = texture(tex0, vec2(_uv0.x, _uv0.y));
	//FragColor = vec3(1, 1, 1);
}