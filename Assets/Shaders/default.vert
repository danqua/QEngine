#version 440 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec2 uv0;
layout(location = 2) in vec2 uv1;
layout(location = 3) in vec2 uv2;
layout(location = 4) in vec2 uv3;
layout(location = 5) in vec3 normal;
layout(location = 6) in vec4 color;

out vec2 _uv0;
out vec2 _uv1;
out vec2 _uv2;
out vec2 _uv3;
out vec3 _normal;

uniform mat4 mvp;
uniform mat4 model;
uniform mat4 view;

void main()
{
	gl_Position = mvp * vec4(position, 1.0);
	_uv0 = uv0;
}