#include "Engine.h"

#define GLM_ENABLE_EXPERIMENTAL

// THIS IS REALLY BAD
#include <Windows.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include <QERenderer/Window.h>
#include <QERenderer/Interfaces/BaseRenderContext.h>

#include <QERenderer/Platform/OpenGL/GLTexture.h>
#include <QERenderer/Platform/OpenGL/GLContext.h>
#include <QERenderer/Platform/OpenGL/GLShader.h>
#include <QERenderer/Platform/OpenGL/GLVertexArray.h>
#include <QERenderer/Platform/OpenGL/GLVertexBuffer.h>
#include <QERenderer/Platform/OpenGL/GLIndexBuffer.h>


#include <QERenderer/Platform/OpenGL/Primitives/GLQuad.h>
#include <QERenderer/Platform/OpenGL/GLRenderer2D.h>

#include <GL/glew.h>


namespace QEngine
{
	Engine::Engine(GRAPHICS_API eApi)
	{
		m_eApi = eApi;
	}

	Engine::~Engine(void)
	{

	}

	void Engine::Init(const std::string& title, int width, int height)
	{
		if (m_eApi == GRAPHICS_API::OPENGL)
		{
			m_pRenderContext	= new Renderer::GLContext;
		}

		m_pWindow = new Renderer::Window;
		m_pWindow->Init(title, width, height);
		m_pRenderContext->Init(m_pWindow);
		m_Camera2D = new Camera2D((float)m_pWindow->GetWidth(), (float)m_pWindow->GetHeight());
		m_Camera2D->SetPosition(glm::vec3(0.0f, 0.0f, 0.0f));
	}

	void Engine::Run(void)
	{

		Renderer::GLRenderer2D ren;

		Renderer::GLQuad quad;
		quad.transform.scale = Vec3(256.0f, 256.0f, 0.0f);
		quad.transform.position = Vec3(m_pWindow->GetWidth() / 2.0f, m_pWindow->GetHeight() / 2.0f, 0.0f);

		Renderer::GLQuad quad2;
		quad2.transform.scale = Vec3(256.0f, 256.0f, 0.0f);
		quad2.transform.position = Vec3(256.0f, 256.0f, 0.0f);

		std::string path = "C:/Users/danqu/CloudStation/Dokumente/Entwicklung/QEngine/Assets/";
		Renderer::GLShader shader;
		shader.Load(path + "Shaders/default.vert", path + "Shaders/default.frag");
		shader.AddUniformLocation("mvp");
		Renderer::GLTexture texture;
		texture.LoadFromFile(path + "Textures/default.png");

		glm::mat4 view = m_Camera2D->GetView();// glm::translate(glm::mat4(1.0f), glm::vec3(0, 0, 0));
		

		float test = 0.0f;




		while (m_pWindow->IsRunning())
		{
			glClear(GL_COLOR_BUFFER_BIT);


			quad.transform.rotation.z += 0.01f;
			quad.Update(0.1666f);
			quad2.Update();

						
			glm::mat4 mvp = m_Camera2D->GetViewProjection() * quad.GetModelMatrix();
			shader.Bind();
			texture.Bind();
			shader.SetUniformLocationMat4("mvp", mvp);
			ren.Draw(&quad);

			mvp = m_Camera2D->GetViewProjection() * quad2.GetModelMatrix();
			
			shader.SetUniformLocationMat4("mvp", mvp);
			ren.Draw(&quad2);
			
			shader.Unbind();
			texture.Unbind();

			m_pWindow->SwapBuffers();
		}
	}
}