#pragma once

#include <string>

#include "Cameras/Camera2D.h"

enum GRAPHICS_API
{
	NONE,
	OPENGL,
	VULKAN
};

namespace QEngine {

	namespace Renderer {
		class BaseWindow;
		class BaseRenderContext;
	}

	class Engine
	{
	public:
		// Constructor
		Engine(GRAPHICS_API eApi);
		// Destructor
		~Engine(void);
		// Initialize the engine
		void Init(const std::string& title, int width, int height);
		// Run the engine
		void Run(void);
	private:
		Renderer::BaseWindow*			m_pWindow;
		Renderer::BaseRenderContext*	m_pRenderContext;
		Camera2D*						m_Camera2D;
		GRAPHICS_API					m_eApi;
	};
}