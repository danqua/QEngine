#pragma once

#include "Interfaces/BaseCamera.h"

namespace QEngine {

	class Camera2D : public BaseCamera
	{
	public:
		Camera2D(void) { }
		Camera2D(float fWidth, float fHeight, float fNear = -1.0f, float fFar = 1.0f);
		~Camera2D();
		virtual void Update(float delta = 1.0f) override;
	};

}
