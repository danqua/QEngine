#include "Camera2D.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>


namespace QEngine {

	Camera2D::Camera2D(float fWidth, float fHeight, float fNear, float fFar)
	{
		m_Position		= glm::vec3(0.0f, 0.0f, 0.0f);
		m_Projection	= glm::ortho(0.0f, fWidth, fHeight, 0.0f, fNear, fFar);
		m_View			= glm::translate(glm::mat4(1.0f), m_Position);
	}

	Camera2D::~Camera2D()
	{

	}

	void Camera2D::Update(float delta)
	{
		m_View = glm::translate(glm::mat4(1.0f), m_Position);
	}

}