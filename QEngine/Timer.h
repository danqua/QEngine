#pragma once

namespace QEngine {

	class Timer
	{
	public:
		
		Timer();
		~Timer();

		void Init(void);
	private:
		std::chrono::steady_clock::time_point start;
	};


}