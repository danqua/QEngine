#pragma once

#include <glm/glm.hpp>

namespace QEngine {

	/**
	 * BaseCamera
	 *
	 * This base class is just a base class for a
	 * more specific camera class like Camera2D or Camera3D.
	 */
	class BaseCamera
	{
	public:
		/**
		 * Default destructor
		 */
		virtual ~BaseCamera(void) = default;
		
		/**
		 * Update
		 *
		 * Updates the camera
		 */
		virtual void Update(float fDelta = 1.0f) = 0;

		/**
		 * Move
		 *
		 * @param direction Moves the camera towards that direction
		 */
		void Move(const glm::vec3& direction) { m_Position += direction; }
		/**
		 * SetPosition
		 *
		 * @param position The position of the camera in x, y, z
		 */
		void SetPosition(const glm::vec3& position) { m_Position = position; }

		/**
		 * GetPosition
		 *
		 * @return The position of the camera (Vector 3d)
		 */
		inline const glm::vec3& GetPosition(void) const { return m_Position; }

		/**
		* GetProjection
		*
		* @return Returns the projection matrix
		*/
		inline const glm::mat4& GetProjection(void) const { return m_Projection; }

		/**
		* GetView
		*
		* @return Returns the projection matrix
		*/
		inline const glm::mat4& GetView(void) const { return m_View; }

		/**
		* GetViewProjection
		*
		* @return Returns the projection matrix
		*/
		glm::mat4 GetViewProjection(void) const { return m_Projection * m_View; }
	protected:
		/** Position of the camera */
		glm::vec3 m_Position;
		/** Projection matrix */
		glm::mat4 m_Projection;
		/** View matrix */
		glm::mat4 m_View;
		/** Camera movement speed */
		float m_MovementSpeed;
	};
}