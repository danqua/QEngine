#pragma once

#ifdef _WIN32
	#ifdef CORE_DLL
		#define QEAPI __declspec(dllexport)
	#else
		#define QEAPI __declspec(dllimport)
	#endif
#endif